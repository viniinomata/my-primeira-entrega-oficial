---
title: "About"
date: 2021-10-13T18:11:47-03:00
draft: false
---

Bem vindo ao Site My Primeira Entrega.
---------------------------------------
Nessa página você encontrará informações sobre o trabalho.

+ O trabalho a seguir foi desenvolvido para a disciplina PMR3304 - Sistemas de Informação
    + Foi usado o gerador de sites estáticos Hugo!!

+ Foi desenvolvido por Vinicius Hideki Inomata - Nusp 10722112
+ Desculpa pela falta de criatividade (╥﹏╥)

Obrigado pela Atenção!! (｡•̀ᴗ-)✧
-----------------------------------
